type: edu
custom_name: stage6
files:
- name: src/machine/CoffeeMachine.java
  visible: true
  text: |
    package machine;

    public class CoffeeMachine {
        public static void main(String[] args) {
            System.out.println("Hello World!");
        }
    }
  learner_created: false
- name: test/CoffeeMachineTest.java
  visible: false
  text: |
    import org.hyperskill.hstest.stage.StageTest;
    import org.hyperskill.hstest.testcase.CheckResult;
    import org.hyperskill.hstest.testcase.TestCase;

    import java.util.ArrayList;
    import java.util.List;


    class TestClue {
        int water;
        int milk;
        int beans;
        int cups;
        int money;
        String feedback;
        TestClue(int w, int m, int b, int c, int mo, String feedback) {
            water = w;
            milk = m;
            beans = b;
            cups = c;
            money = mo;
            this.feedback = feedback;
        }
    }

    public class CoffeeMachineTest extends StageTest<TestClue> {

        @Override
        public List<TestCase<TestClue>> generate() {
            return List.of(
                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        700 - 400,
                        390 - 540,
                        80 - 120 ,
                        7 - 9,
                        0 - 550,
                        "This test is exactly " +
                            "like in the example - try to run it by yourself"))
                    .setInput(
                        "remaining\n" +
                            "buy\n" +
                            "2\n" +
                            "buy\n" +
                            "2\n" +
                            "fill\n" +
                            "1000\n" +
                            "0\n" +
                            "0\n" +
                            "0\n" +
                            "buy\n" +
                            "2\n" +
                            "take\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        3000,
                        3000,
                        3000 ,
                        3000,
                        0,
                        "This test checks \"fill\" action"))
                    .setInput(
                        "remaining\n" +
                            "fill\n" +
                            "3000\n" +
                            "3000\n" +
                            "3000\n" +
                            "3000\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        -250,
                        0,
                        -16 ,
                        -1,
                        4, "This test checks \"buy\" " +
                        "action with the first variant of coffee"))
                    .setInput(
                        "remaining\n" +
                            "buy\n" +
                            "1\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        -350,
                        -75,
                        -20 ,
                        -1,
                        7, "This test checks \"buy\" " +
                        "action with the second variant of coffee"))
                    .setInput(
                        "remaining\n" +
                            "buy\n" +
                            "2\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        -200,
                        -100,
                        -12 ,
                        -1,
                        6, "This test checks \"buy\" " +
                        "action with the third variant of coffee"))
                    .setInput(
                        "remaining\n" +
                            "buy\n" +
                            "3\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        0,
                        0,
                        0 ,
                        0,
                        -550, "This test checks \"take\" action"))
                    .setInput(
                        "remaining\n" +
                            "take\n" +
                            "remaining\n" +
                            "exit\n"),

                new TestCase<TestClue>()
                    .setAttach(new TestClue(
                        0,
                        0,
                        0 ,
                        0,
                        0, "This test checks \"back\" " +
                        "action right after \"buy\" action"))
                    .setInput(
                        "remaining\n" +
                            "buy\n" +
                            "back\n" +
                            "remaining\n" +
                            "exit\n")
            );
        }

        @Override
        public CheckResult check(String reply, TestClue clue) {
            String[] lines = reply.split("\\n");

            if (lines.length <= 1) {
                return CheckResult.wrong("Looks like you didn't print anything!");
            }

            int water_ = clue.water;
            int milk_ = clue.milk;
            int beans_ = clue.beans;
            int cups_ = clue.cups;
            int money_ = clue.money;

            List<Integer> milk = new ArrayList<>();
            List<Integer> water = new ArrayList<>();
            List<Integer> beans = new ArrayList<>();
            List<Integer> cups = new ArrayList<>();
            List<Integer> money = new ArrayList<>();

            for (String line : lines) {
                line = line.replace("$", "").trim();
                String[] words = line.split("\\s+");
                if (words.length == 0) {
                    continue;
                }
                String firstWord = words[0];
                int amount;
                try {
                    amount = Integer.parseInt(firstWord);
                }
                catch (Exception e) {
                    continue;
                }
                if (line.contains("milk")) {
                    milk.add(amount);
                }
                else if (line.contains("water")) {
                    water.add(amount);
                }
                else if (line.contains("beans")) {
                    beans.add(amount);
                }
                else if (line.contains("cups")) {
                    cups.add(amount);
                }
                else if (line.contains("money")) {
                    money.add(amount);
                }
            }

            if (milk.size() != 2) {
                return new CheckResult(false,
                    "There should be two lines with \"milk\", " +
                        "found: " + milk.size());
            }

            if (water.size() != 2) {
                return new CheckResult(false,
                    "There should be two lines with \"water\", " +
                        "found: " + water.size());
            }

            if (beans.size() != 2) {
                return new CheckResult(false,
                    "There should be two lines with \"beans\", " +
                        "found: " + beans.size());
            }

            if (cups.size() != 2) {
                return new CheckResult(false,
                    "There should be two lines with \"cups\", " +
                        "found: " + cups.size());
            }

            if (money.size() != 2) {
                return new CheckResult(false,
                    "There should be two lines with \"money\", " +
                        "found: " + money.size());
            }

            int milk0 = milk.get(0);
            int milk1 = milk.get(milk.size() - 1);

            int water0 = water.get(0);
            int water1 = water.get(water.size() - 1);

            int beans0 = beans.get(0);
            int beans1 = beans.get(beans.size() - 1);

            int cups0 = cups.get(0);
            int cups1 = cups.get(cups.size() - 1);

            int money0 = money.get(0);
            int money1 = money.get(money.size() - 1);

            int diffWater = water1 - water0;
            int diffMilk = milk1 - milk0;
            int diffBeans = beans1 - beans0;
            int diffCups = cups1 - cups0;
            int diffMoney = money1 - money0;

            boolean isCorrect =
                diffWater == water_ &&
                    diffMilk == milk_ &&
                    diffBeans == beans_ &&
                    diffCups == cups_ &&
                    diffMoney == money_;

            return new CheckResult(isCorrect, clue.feedback);
        }
    }
  learner_created: false
- name: src/machine/ResourceSupply.java
  visible: true
  text: |
    package machine;

    public class ResourceSupply {
        private int availableWaterMl;
        private int availableMilkMl;
        private int availableCoffeeBeansG;
        private int disposableCups;
        private int money;

        public ResourceSupply(int availableWaterMl, int availableMilkMl, int availableCoffeeBeansG, int disposableCups, int money) {
            this.availableWaterMl = availableWaterMl;
            this.availableMilkMl = availableMilkMl;
            this.availableCoffeeBeansG = availableCoffeeBeansG;
            this.disposableCups = disposableCups;
            this.money = money;
        }

        public int getAvailableWaterMl() {
            return availableWaterMl;
        }

        public void setAvailableWaterMl(int availableWaterMl) {
            this.availableWaterMl = availableWaterMl;
        }

        public int getAvailableMilkMl() {
            return availableMilkMl;
        }

        public void setAvailableMilkMl(int availableMilkMl) {
            this.availableMilkMl = availableMilkMl;
        }

        public int getAvailableCoffeeBeansG() {
            return availableCoffeeBeansG;
        }

        public void setAvailableCoffeeBeansG(int availableCoffeeBeansG) {
            this.availableCoffeeBeansG = availableCoffeeBeansG;
        }

        public int getDisposableCups() {
            return disposableCups;
        }

        public void setDisposableCups(int disposableCups) {
            this.disposableCups = disposableCups;
        }

        public int getMoney() {
            return money;
        }

        public void setMoney(int money) {
            this.money = money;
        }

        @Override
        public String toString() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("\nThe coffee machine has:\n");
            stringBuilder.append(String.format("%d ml of water\n", this.availableWaterMl));
            stringBuilder.append(String.format("%d ml of milk\n", this.availableMilkMl));
            stringBuilder.append(String.format("%d g of coffee beans\n", this.availableCoffeeBeansG));
            stringBuilder.append(String.format("%d disposable cups\n", this.disposableCups));
            stringBuilder.append(String.format("$%d of money\n\n", this.money));
            return stringBuilder.toString();
        }
    }
  learner_created: true
- name: src/machine/MachineAnswer.java
  visible: true
  text: |
    package machine;

    public enum MachineAnswer {
        NOT_ENOUGH_WATER,
        NOT_ENOUGH_MILK,
        NOT_ENOUGH_BEAN,
        NOT_ENOUGH_CUP,
        ABLE_TO_SERVE;
    }
  learner_created: true
- name: src/machine/CoffeeCategory.java
  visible: true
  text: |
    package machine;

    import java.util.Map;

    public enum CoffeeCategory {
        ESPRESSO (Map.of(
                CoffeeCategoryKey.WATER_ML, -250,
                CoffeeCategoryKey.MILK_ML, 0,
                CoffeeCategoryKey.BEAN_GRAM, -16,
                CoffeeCategoryKey.DISPOSABLE_CUP, -1,
                CoffeeCategoryKey.MONEY, 4
        )),
        LATTE (Map.of(
                CoffeeCategoryKey.WATER_ML, -350,
                CoffeeCategoryKey.MILK_ML, -75,
                CoffeeCategoryKey.BEAN_GRAM, -20,
                CoffeeCategoryKey.DISPOSABLE_CUP, -1,
                CoffeeCategoryKey.MONEY, 7
        )),
        CAPPUCCINO (Map.of(
                CoffeeCategoryKey.WATER_ML, -200,
                CoffeeCategoryKey.MILK_ML, -100,
                CoffeeCategoryKey.BEAN_GRAM, -12,
                CoffeeCategoryKey.DISPOSABLE_CUP, -1,
                CoffeeCategoryKey.MONEY, 6
        ));

        public final Map<String, Integer> properties;

        CoffeeCategory(Map<String, Integer> properties) {
            this.properties = properties;
        }

        public Map<String, Integer> getProperties() {
            return this.properties;
        }


        public static class CoffeeCategoryKey {
            static final String WATER_ML = "water-ml";
            static final String MILK_ML = "milk-ml";
            static final String BEAN_GRAM = "bean-gram";
            static final String DISPOSABLE_CUP = "disposable-cup";
            static final String MONEY = "money";
        }
    }
  learner_created: true
- name: src/machine/ResourceProcessor.java
  visible: true
  text: "package machine;\n\nimport java.util.Map;\nimport java.util.Scanner;\n\n\
    public class ResourceProcessor {\n\n    private ResourceSupply resourceSupply;\n\
    \    private final int cupsCoffee;\n\n    public ResourceProcessor(ResourceSupply\
    \ resourceSupply, int cupsCoffee) {\n        this.resourceSupply = resourceSupply;\n\
    \        this.cupsCoffee = cupsCoffee;\n    }\n\n    public void summaryResources()\
    \ {\n        System.out.print(this.resourceSupply.toString());\n    }\n\n    //\
    \ parameterized by using lambda function?\n    public void handleUserInput(String\
    \ action) {\n        switch (action) {\n            case \"buy\":\n          \
    \      makeCoffee();\n                break;\n            case \"fill\":\n   \
    \             fillResource();\n                break;\n            case \"take\"\
    :\n                takeMoney();\n                break;\n            case \"remaining\"\
    :\n                summaryResources();\n                break;\n            default:\n\
    \                break;\n        }\n    }\n\n    private void takeMoney() {\n\
    \        int withdrawnMoney = this.resourceSupply.getMoney();\n        System.out.println();\n\
    \        System.out.printf(\"I gave you $%d\\n\", withdrawnMoney);\n        System.out.println();\n\
    \        this.resourceSupply = updateResourceSupply(0, 0, 0, 0, withdrawnMoney\
    \ * -1);\n    }\n\n    private void fillResource() {\n        Scanner input =\
    \ new Scanner(System.in);\n        System.out.println();\n        System.out.println(\"\
    Write how many ml of water you want to add:\");\n        int addWaterMl = input.nextInt();\n\
    \        System.out.println(\"Write how many ml of milk you want to add:\");\n\
    \        int addMilkMl = input.nextInt();\n        System.out.println(\"Write\
    \ how many grams of coffee beans you want to add:\");\n        int addCoffeeBeansG\
    \ = input.nextInt();\n        System.out.println(\"Write how many disposable cups\
    \ of coffee you want to add:\");\n        int addDisposableCups = input.nextInt();\n\
    \        System.out.println();\n        this.resourceSupply = updateResourceSupply(addWaterMl,\
    \ addMilkMl, addCoffeeBeansG, addDisposableCups, 0);\n    }\n\n    private ResourceSupply\
    \ updateResourceSupply(int waterMl, int milkMl, int coffeeBeansG, int disposableCups,\
    \ int money) {\n        return new ResourceSupply(\n                this.resourceSupply.getAvailableWaterMl()\
    \ + waterMl,\n                this.resourceSupply.getAvailableMilkMl() + milkMl,\n\
    \                this.resourceSupply.getAvailableCoffeeBeansG() + coffeeBeansG,\n\
    \                this.resourceSupply.getDisposableCups() + disposableCups,\n \
    \               this.resourceSupply.getMoney() + money\n        );\n    }\n  \
    \  \n    private ResourceSupply updateResourceSupply(CoffeeCategory coffeeCategory)\
    \ {\n        Map<String, Integer> properties = coffeeCategory.getProperties();\n\
    \n        return updateResourceSupply(\n                properties.get(CoffeeCategory.CoffeeCategoryKey.WATER_ML),\n\
    \                properties.get(CoffeeCategory.CoffeeCategoryKey.MILK_ML),\n \
    \               properties.get(CoffeeCategory.CoffeeCategoryKey.BEAN_GRAM),\n\
    \                properties.get(CoffeeCategory.CoffeeCategoryKey.DISPOSABLE_CUP),\n\
    \                properties.get(CoffeeCategory.CoffeeCategoryKey.MONEY)\n    \
    \    );\n    }\n\n    private void makeCoffee() {\n        Scanner scanner = new\
    \ Scanner(System.in);\n        System.out.println(\"\\nWhat do you want to buy?\
    \ 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:\");\n        String\
    \ selection = scanner.next().trim();\n        switch (selection) {\n         \
    \   case \"1\":\n                brewCoffee(CoffeeCategory.ESPRESSO);\n      \
    \          break;\n            case \"2\":\n                brewCoffee(CoffeeCategory.LATTE);\n\
    \                break;\n            case \"3\":\n                brewCoffee(CoffeeCategory.CAPPUCCINO);\n\
    \                break;\n            default:\n                break;\n      \
    \  }\n    }\n\n    private void brewCoffee(CoffeeCategory category) {\n      \
    \  switch (isAbleToServe(category)) {\n            case NOT_ENOUGH_WATER:\n  \
    \              System.out.println(\"Sorry, not enough water!\");\n           \
    \     break;\n            case NOT_ENOUGH_MILK:\n                System.out.println(\"\
    Sorry, not enough milk!\");\n                break;\n            case NOT_ENOUGH_BEAN:\n\
    \                System.out.println(\"Sorry, not enough bean!\");\n          \
    \      break;\n            case NOT_ENOUGH_CUP:\n                System.out.println(\"\
    Sorry, not enough cup!\");\n                break;\n            case ABLE_TO_SERVE:\n\
    \                System.out.println(\"I have enough resources, making you a coffee!\\\
    n\");\n                this.resourceSupply = updateResourceSupply(category);\n\
    \                break;\n            default:\n                break;\n      \
    \  }\n    }\n\n    private MachineAnswer isAbleToServe(CoffeeCategory coffeeCategory)\
    \ {\n        if (!hasEnoughWater(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.WATER_ML))))\
    \ {\n            return MachineAnswer.NOT_ENOUGH_WATER;\n        } else if (!hasEnoughMilk(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.MILK_ML))))\
    \ {\n            return MachineAnswer.NOT_ENOUGH_MILK;\n        } else if (!hasEnoughBean(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.BEAN_GRAM))))\
    \ {\n            return MachineAnswer.NOT_ENOUGH_BEAN;\n        } else if (!hasEnoughCup(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.DISPOSABLE_CUP))))\
    \ {\n            return MachineAnswer.NOT_ENOUGH_CUP;\n        }\n\n        return\
    \ MachineAnswer.ABLE_TO_SERVE;\n    }\n\n    private boolean hasEnoughWater(int\
    \ demandedWater) {\n        return resourceSupply.getAvailableWaterMl() >= demandedWater;\n\
    \    }\n\n    private boolean hasEnoughMilk(int demandedMilk) {\n        return\
    \ resourceSupply.getAvailableMilkMl() >= demandedMilk;\n    }\n\n    private boolean\
    \ hasEnoughBean(int demandedBean) {\n        return resourceSupply.getAvailableCoffeeBeansG()\
    \ >= demandedBean;\n    }\n\n    private boolean hasEnoughCup(int demandedCup)\
    \ {\n        return resourceSupply.getDisposableCups() >= demandedCup;\n    }\n\
    }\n"
  learner_created: true
feedback_link: https://hyperskill.org/projects/33/stages/180/implement#comment
status: Solved
feedback:
  message: Well done! You've finished the project. Select a new project on <a href="https://hyperskill.org/projects">JetBrains
    Academy</a> to continue learning.
  time: Fri, 10 Dec 2021 11:47:39 UTC
record: -1
