package machine;

import java.util.Map;

public enum CoffeeCategory {
    ESPRESSO (Map.of(
            CoffeeCategoryKey.WATER_ML, -250,
            CoffeeCategoryKey.MILK_ML, 0,
            CoffeeCategoryKey.BEAN_GRAM, -16,
            CoffeeCategoryKey.DISPOSABLE_CUP, -1,
            CoffeeCategoryKey.MONEY, 4
    )),
    LATTE (Map.of(
            CoffeeCategoryKey.WATER_ML, -350,
            CoffeeCategoryKey.MILK_ML, -75,
            CoffeeCategoryKey.BEAN_GRAM, -20,
            CoffeeCategoryKey.DISPOSABLE_CUP, -1,
            CoffeeCategoryKey.MONEY, 7
    )),
    CAPPUCCINO (Map.of(
            CoffeeCategoryKey.WATER_ML, -200,
            CoffeeCategoryKey.MILK_ML, -100,
            CoffeeCategoryKey.BEAN_GRAM, -12,
            CoffeeCategoryKey.DISPOSABLE_CUP, -1,
            CoffeeCategoryKey.MONEY, 6
    ));

    public final Map<String, Integer> properties;

    CoffeeCategory(Map<String, Integer> properties) {
        this.properties = properties;
    }

    public Map<String, Integer> getProperties() {
        return this.properties;
    }


    public static class CoffeeCategoryKey {
        static final String WATER_ML = "water-ml";
        static final String MILK_ML = "milk-ml";
        static final String BEAN_GRAM = "bean-gram";
        static final String DISPOSABLE_CUP = "disposable-cup";
        static final String MONEY = "money";
    }
}
