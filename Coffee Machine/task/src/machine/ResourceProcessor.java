package machine;

import java.util.Map;
import java.util.Scanner;

public class ResourceProcessor {

    private ResourceSupply resourceSupply;

    public ResourceProcessor(ResourceSupply resourceSupply) {
        this.resourceSupply = resourceSupply;
    }

    public void summaryResources() {
        System.out.print(this.resourceSupply.toString());
    }

    public void takeMoney() {
        int withdrawnMoney = this.resourceSupply.getMoney();
        System.out.println();
        System.out.printf("I gave you $%d\n", withdrawnMoney);
        System.out.println();
        this.resourceSupply = updateResourceSupply(0, 0, 0, 0, withdrawnMoney * -1);
    }

    public void fillResource(int addWaterMl, int addMilkMl, int addCoffeeBeansG, int addDisposableCups) {
        this.resourceSupply = updateResourceSupply(addWaterMl, addMilkMl, addCoffeeBeansG, addDisposableCups, 0);
    }

    private ResourceSupply updateResourceSupply(int waterMl, int milkMl, int coffeeBeansG, int disposableCups, int money) {
        return new ResourceSupply(
                this.resourceSupply.getAvailableWaterMl() + waterMl,
                this.resourceSupply.getAvailableMilkMl() + milkMl,
                this.resourceSupply.getAvailableCoffeeBeansG() + coffeeBeansG,
                this.resourceSupply.getDisposableCups() + disposableCups,
                this.resourceSupply.getMoney() + money
        );
    }
    
    private ResourceSupply updateResourceSupply(CoffeeCategory coffeeCategory) {
        Map<String, Integer> properties = coffeeCategory.getProperties();

        return updateResourceSupply(
                properties.get(CoffeeCategory.CoffeeCategoryKey.WATER_ML),
                properties.get(CoffeeCategory.CoffeeCategoryKey.MILK_ML),
                properties.get(CoffeeCategory.CoffeeCategoryKey.BEAN_GRAM),
                properties.get(CoffeeCategory.CoffeeCategoryKey.DISPOSABLE_CUP),
                properties.get(CoffeeCategory.CoffeeCategoryKey.MONEY)
        );
    }

    public void makeCoffee(String selection) {
        switch (selection) {
            case "1":
                brewCoffee(CoffeeCategory.ESPRESSO);
                break;
            case "2":
                brewCoffee(CoffeeCategory.LATTE);
                break;
            case "3":
                brewCoffee(CoffeeCategory.CAPPUCCINO);
                break;
            default:
                break;
        }
    }

    private void brewCoffee(CoffeeCategory category) {
        switch (isAbleToServe(category)) {
            case NOT_ENOUGH_WATER:
                System.out.println("Sorry, not enough water!");
                break;
            case NOT_ENOUGH_MILK:
                System.out.println("Sorry, not enough milk!");
                break;
            case NOT_ENOUGH_BEAN:
                System.out.println("Sorry, not enough bean!");
                break;
            case NOT_ENOUGH_CUP:
                System.out.println("Sorry, not enough cup!");
                break;
            case ABLE_TO_SERVE:
                System.out.println("I have enough resources, making you a coffee!\n");
                this.resourceSupply = updateResourceSupply(category);
                break;
            default:
                break;
        }
    }

    private MachineAnswer isAbleToServe(CoffeeCategory coffeeCategory) {
        if (!hasEnoughWater(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.WATER_ML)))) {
            return MachineAnswer.NOT_ENOUGH_WATER;
        } else if (!hasEnoughMilk(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.MILK_ML)))) {
            return MachineAnswer.NOT_ENOUGH_MILK;
        } else if (!hasEnoughBean(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.BEAN_GRAM)))) {
            return MachineAnswer.NOT_ENOUGH_BEAN;
        } else if (!hasEnoughCup(Math.abs(coffeeCategory.getProperties().get(CoffeeCategory.CoffeeCategoryKey.DISPOSABLE_CUP)))) {
            return MachineAnswer.NOT_ENOUGH_CUP;
        }

        return MachineAnswer.ABLE_TO_SERVE;
    }

    private boolean hasEnoughWater(int demandedWater) {
        return resourceSupply.getAvailableWaterMl() >= demandedWater;
    }

    private boolean hasEnoughMilk(int demandedMilk) {
        return resourceSupply.getAvailableMilkMl() >= demandedMilk;
    }

    private boolean hasEnoughBean(int demandedBean) {
        return resourceSupply.getAvailableCoffeeBeansG() >= demandedBean;
    }

    private boolean hasEnoughCup(int demandedCup) {
        return resourceSupply.getDisposableCups() >= demandedCup;
    }
}
