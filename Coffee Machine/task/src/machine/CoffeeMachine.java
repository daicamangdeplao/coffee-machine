package machine;

import java.util.Scanner;

public class CoffeeMachine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ResourceProcessor resourceProcessor = new ResourceProcessor(
                new ResourceSupply(400, 540, 120, 9, 550)
        );
        while (true) {
            System.out.println("Write action (buy, fill, take, remaining, exit):");
            String action = scanner.next().trim();
            handleUserInteraction(scanner, resourceProcessor, action);
            if (action.equals("exit")) {
                break;
            }
        }
    }

    private static void handleUserInteraction(Scanner scanner, ResourceProcessor resourceProcessor, String action) {
        switch (action) {
            case "buy":
                handleBuySelection(scanner, resourceProcessor);
                break;
            case "fill":
                handleFillSelection(scanner, resourceProcessor);
                break;
            case "take":
                resourceProcessor.takeMoney();
                break;
            case "remaining":
                resourceProcessor.summaryResources();
                break;
            default:
                break;
        }
    }

    private static void handleFillSelection(Scanner scanner, ResourceProcessor resourceProcessor) {
        System.out.println();
        System.out.println("Write how many ml of water you want to add:");
        int addWaterMl = scanner.nextInt();
        System.out.println("Write how many ml of milk you want to add:");
        int addMilkMl = scanner.nextInt();
        System.out.println("Write how many grams of coffee beans you want to add:");
        int addCoffeeBeansG = scanner.nextInt();
        System.out.println("Write how many disposable cups of coffee you want to add:");
        int addDisposableCups = scanner.nextInt();
        System.out.println();
        resourceProcessor.fillResource(addWaterMl, addMilkMl, addCoffeeBeansG, addDisposableCups);
    }

    private static void handleBuySelection(Scanner scanner, ResourceProcessor resourceProcessor) {
        System.out.println("\nWhat do you want to buy? 1 - espresso, 2 - latte, 3 - cappuccino, back - to main menu:");
        String selection = scanner.next().trim();
        resourceProcessor.makeCoffee(selection);
    }
}
