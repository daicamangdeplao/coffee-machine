package machine;

public class ResourceSupply {
    private int availableWaterMl;
    private int availableMilkMl;
    private int availableCoffeeBeansG;
    private int disposableCups;
    private int money;

    public ResourceSupply(int availableWaterMl, int availableMilkMl, int availableCoffeeBeansG, int disposableCups, int money) {
        this.availableWaterMl = availableWaterMl;
        this.availableMilkMl = availableMilkMl;
        this.availableCoffeeBeansG = availableCoffeeBeansG;
        this.disposableCups = disposableCups;
        this.money = money;
    }

    public int getAvailableWaterMl() {
        return availableWaterMl;
    }

    public void setAvailableWaterMl(int availableWaterMl) {
        this.availableWaterMl = availableWaterMl;
    }

    public int getAvailableMilkMl() {
        return availableMilkMl;
    }

    public void setAvailableMilkMl(int availableMilkMl) {
        this.availableMilkMl = availableMilkMl;
    }

    public int getAvailableCoffeeBeansG() {
        return availableCoffeeBeansG;
    }

    public void setAvailableCoffeeBeansG(int availableCoffeeBeansG) {
        this.availableCoffeeBeansG = availableCoffeeBeansG;
    }

    public int getDisposableCups() {
        return disposableCups;
    }

    public void setDisposableCups(int disposableCups) {
        this.disposableCups = disposableCups;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\nThe coffee machine has:\n");
        stringBuilder.append(String.format("%d ml of water\n", this.availableWaterMl));
        stringBuilder.append(String.format("%d ml of milk\n", this.availableMilkMl));
        stringBuilder.append(String.format("%d g of coffee beans\n", this.availableCoffeeBeansG));
        stringBuilder.append(String.format("%d disposable cups\n", this.disposableCups));
        stringBuilder.append(String.format("$%d of money\n\n", this.money));
        return stringBuilder.toString();
    }
}
