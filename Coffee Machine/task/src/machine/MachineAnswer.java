package machine;

public enum MachineAnswer {
    NOT_ENOUGH_WATER,
    NOT_ENOUGH_MILK,
    NOT_ENOUGH_BEAN,
    NOT_ENOUGH_CUP,
    ABLE_TO_SERVE;
}
