class Cat {

    // write static and instance variables
    String name;
    int age;
    static int counter;


    public Cat(String name, int age) {
        // implement the constructor
        // do not forget to check the number of cats
        this.name = name;
        this.age = age;
        counter++;
        if (getNumberOfCats() > 5) {
            System.out.println("You have too many cats");
        }
    }

    public static int getNumberOfCats() {
        // implement the static method
        return counter;
    }
}

//class Main {
//    public static void main(String[] args) {
//        Cat cat1 = new Cat("a", 1);
//        Cat cat2 = new Cat("b", 1);
//        Cat cat3 = new Cat("c", 1);
//        Cat cat4 = new Cat("d", 1);
//        Cat cat5 = new Cat("e", 1);
//        Cat cat6 = new Cat("f", 1);
//        Cat cat7 = new Cat("g", 1);
//    }
//}
