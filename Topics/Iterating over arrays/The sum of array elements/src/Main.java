import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        String length = scanner.nextLine();
        String[] numbers = scanner.nextLine().split(" ");

        int sum = Arrays.stream(numbers).mapToInt(Integer::parseInt).sum();
        System.out.print(sum);
    }
}
