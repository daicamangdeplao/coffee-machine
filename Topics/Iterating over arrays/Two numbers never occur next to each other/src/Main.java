import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        int length = Integer.parseInt(scanner.nextLine());
        String[] numbers = scanner.nextLine().split(" ");
        String[] candidates = scanner.nextLine().split(" ");

        boolean result = true;
        for (int i = 0; i < length - 1; i++) {
            if ((numbers[i].equals(candidates[0]) && numbers[i + 1].equals(candidates[1]) ||
                    (numbers[i].equals(candidates[1]) && numbers[i + 1].equals(candidates[0])))) {
                result = false;
                break;
            }
        }

        System.out.println(result);
    }
}
