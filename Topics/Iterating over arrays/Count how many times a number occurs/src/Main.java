import java.util.Arrays;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        // put your code here
        Scanner scanner = new Scanner(System.in);
        String length = scanner.nextLine();
        String[] numbers = scanner.nextLine().split(" ");
        String candidate = scanner.nextLine();

        int result = (int) Arrays.stream(numbers)
                .filter(x -> x.equals(candidate))
                .count();

        System.out.println(result);
    }
}
