class Circle {

    double radius;

    // write methods here
    double getLength() {
        return radius * 2 * Math.PI;
    }

    double getArea() {
        return Math.pow(radius, 2) * Math.PI;
    }
}
