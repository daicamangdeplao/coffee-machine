import java.util.Scanner;

public class Main {

    public static boolean isVowel(char ch) {
        // write your code here
        String str = String.valueOf(ch).toLowerCase();
        if (str.equals("a") ||
                str.equals("e") ||
                str.equals("u") ||
                str.equals("i") ||
                str.equals("o")
        ) {
            return true;
        }
        return false;
    }

    /* Do not change code below */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        char letter = scanner.nextLine().charAt(0);
        System.out.println(isVowel(letter) ? "YES" : "NO");
    }
}
