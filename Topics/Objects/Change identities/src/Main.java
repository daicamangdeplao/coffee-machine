class Person {
    String name;
    int age;
}

class MakingChanges {
    public static void changeIdentities(Person p1, Person p2) {
        // write your code here
        String nameP1 = p1.name;
        int ageP1 = p1.age;

        String nameP2 = p2.name;
        int ageP2 = p2.age;

        p1.name = nameP2;
        p1.age = ageP2;

        p2.name = nameP1;
        p2.age = ageP1;
    }
}
