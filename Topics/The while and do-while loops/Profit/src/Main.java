import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
//        int m = 100;
//        int p = 15;
//        int k = 120;
        int m = scanner.nextInt();
        int p = scanner.nextInt();
        int k = scanner.nextInt();

        int years = calculateSavingYears(m, p, k);

        System.out.print(years);
    }

    private static int calculateSavingYears(int deposit, int interestRate, int expectedReturn) {
        int years = 0;
        double sum = deposit;
        while (sum < expectedReturn) {
            sum = sum + (sum * (double) interestRate / 100.0);
            years = years + 1;
        }
        return years;
    }
}
