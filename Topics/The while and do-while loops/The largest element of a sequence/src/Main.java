import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        int number;
        List<Integer> numbers = new ArrayList<>();

        while (scanner.hasNext()) {
            number = scanner.nextInt();
            if (number == 0) {
                break;
            } else {
                numbers.add(number);
            }
        }
        System.out.print(numbers.parallelStream().sorted().collect(Collectors.toList()).get(numbers.size() - 1));
    }
}
