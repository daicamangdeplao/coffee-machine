import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        int n = scanner.nextInt();

        System.out.print(n);
        while (n != 1) {
            n = calculate(n);
            System.out.print(" ");
            System.out.print(n);
        }
    }

    private static int calculate(int n) {
        if (isEven(n)) {
            n = n / 2;
        } else {
            n = n * 3 + 1;
        }
        return n;
    }

    public static boolean isEven(int n) {
        return n % 2 == 0;
    }
}
