import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
//        long m = 6_188_989_133L;
        long m = scanner.nextLong();
        long result = 1;
        findResult(m, result);
    }

    private static void findResult(long m, long result) {
        while (true) {
            if (calculateFactorials(result) > m) {
                System.out.println(result);
                break;
            }
            result = result + 1;
        }
    }

    private static long calculateFactorials(long n) {
        long result = 1;
        while (n > 1) {
            result = result * n;
            n = n - 1;
        }
//        System.out.printf("=== factorial of %d is %d\n", n, result);
        return result;
    }
}
