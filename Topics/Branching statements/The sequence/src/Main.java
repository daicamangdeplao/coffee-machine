import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class Main {
    public static void main(String[] args) {
        List<Integer> results = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        // start coding here
        int n = scanner.nextInt();

        for (int i = 1; i < Integer.MAX_VALUE; i++) {
            if (hasReachedMaximumQuota(results, n)) {
                break;
            }
            results.addAll(calculateRepetition(i));
        }
        printResult(results.subList(0, n));
    }

    private static boolean hasReachedMaximumQuota(List<Integer> results, int n) {
        return results.size() >= n;
    }

    private static void printResult(List<Integer> results) {
        results.forEach(v -> System.out.printf("%d ", v));
    }

    private static List<Integer> calculateRepetition(int printNumber) {
        List<Integer> ret = new ArrayList<>();
        for (int i = 1; i <= printNumber; i++) {
            ret.add(printNumber);
        }
        return ret;
    }

}
