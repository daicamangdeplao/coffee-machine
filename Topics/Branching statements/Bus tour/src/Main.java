import java.util.*;
import java.util.stream.Collectors;

class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        // start coding here
        Map<String, Integer> arguments = readFirstInputLine(scanner);
        int busHeight = arguments.get("busHeight");
        boolean crashed = false;
        List<Integer> heights = Arrays.stream(scanner.nextLine().split(" ")).map(Integer::parseInt).collect(Collectors.toList());

        for (int i = 0; i < heights.size(); i++) {
            if (heights.get(i) <= busHeight) {
                System.out.printf("Will crash on bridge %d\n", i + 1);
                crashed = true;
                break;
            }
        }

        if (!crashed) {
            System.out.print("Will not crash");
        }

    }

    private static Map<String, Integer> readFirstInputLine(Scanner scanner) {
        Map<String, Integer> arguments = new HashMap<>();
        String[] firstLineArguments = scanner.nextLine().split(" ");

        arguments.put("busHeight", Integer.parseInt(firstLineArguments[0]));
        arguments.put("numberBridges", Integer.parseInt(firstLineArguments[1]));
        return arguments;
    }
}
